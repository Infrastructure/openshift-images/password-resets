from unittest.mock import MagicMock, AsyncMock
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import declarative_base

import sys


Base = declarative_base()


class TokenMock(Base):
    __tablename__ = 'tokens'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True)
    token = Column(String, index=True)
    expired = Column(Integer, default=0)
    claimed = Column(Integer, default=0)
    date = Column(DateTime)


mock_base = MagicMock()
mock_session = AsyncMock()
mock_token = TokenMock

async_session_context = AsyncMock()
async_session_context.__aenter__.return_value = mock_session
async_session_context.__aexit__.return_value = None

mock_async_session = MagicMock()
mock_async_session.return_value = async_session_context

mock_db_module = MagicMock()
mock_db_module.Token = TokenMock

mock_base_module = MagicMock()
mock_base_module.Base = Base
mock_base_module.Session = mock_async_session

sys.modules['app.helpers.db.db'] = mock_db_module
sys.modules['app.helpers.db.base'] = mock_base_module
