import pytest

from app.worker import send_email, activate_account

TEST_USERNAME = "testuser"
TEST_EMAIL = "noreply@gnome.org"
TEST_TOKEN = "5ad3ee81c6c06a5dc72d7fe4a2286ed2"


@pytest.mark.asyncio
async def test_send_email(mock_smtp):
    send_email(TEST_EMAIL, TEST_TOKEN)

    mock_smtp.starttls.assert_called_once()
    mock_smtp.send_message.assert_called_once()
    mock_smtp.quit.assert_called_once()

    sent_message = mock_smtp.send_message.call_args[0][0]

    assert sent_message['Subject'] == 'GNOME Account Password Reset'
    assert sent_message['From'] == 'GNOME Accounts <noreply@gnome.org>'
    assert sent_message['To'] == TEST_EMAIL
    assert sent_message['Reply-To'] == 'gnome-sysadmin@gnome.org'


@pytest.mark.asyncio
async def test_activate_user(mock_glu):
    activate_account(TEST_USERNAME)

    mock_glu.set_nsaccountlock.assert_called_once_with(TEST_USERNAME, False)
    mock_glu.add_user_to_ldap_group.assert_called_once_with(
        TEST_USERNAME, 'ipausers'
        )
