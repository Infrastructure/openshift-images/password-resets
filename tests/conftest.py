import os
import pytest

from unittest.mock import patch, MagicMock
from tests.mock_ldap import mock_glu as glu
from tests.mock_db import mock_async_session as async_session, TokenMock


EXCLUDED_USERS = 'testusername'
os.environ['EXCLUDED_USERS'] = EXCLUDED_USERS


@pytest.fixture
def mock_glu():
    return glu


@pytest.fixture()
def mock_async_session():
    with patch('app.main.async_session', async_session) as mock:
        mock.return_value.__aenter__.return_value.commit.reset_mock()
        yield mock


@pytest.fixture()
def mock_send_email():
    with patch('app.worker.send_email.send') as mock:
        mock.return_value = None
        yield mock


@pytest.fixture(autouse=True)
def mock_activate_user():
    with patch('app.worker.activate_account.send') as mock:
        mock.return_value = None
        yield mock


@pytest.fixture
def mock_token():
    return TokenMock


@pytest.fixture
def mock_smtp():
    with patch('smtplib.SMTP') as mock:
        smtp_instance = MagicMock()
        mock.return_value.__enter__.return_value = smtp_instance
        yield smtp_instance
