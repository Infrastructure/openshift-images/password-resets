from unittest.mock import MagicMock
import sys

mock_glu = MagicMock()

mock_glu_module = MagicMock()
mock_glu_module.GnomeLdapUtils = MagicMock(return_value=mock_glu)
sys.modules['app.helpers.glu.gnome_ldap_utils'] = mock_glu_module
