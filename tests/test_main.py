import pytest
import httpx

from fastapi.testclient import TestClient
from unittest.mock import MagicMock
from app.main import app
from app.helpers.settings.config import settings

client = TestClient(app)

TEST_USERNAME = "testusername"
TEST_EMAIL = "noreply@gnome.org"
EXCLUDED_USERS = "testusername1"


def test_form_get():
    response = client.get("/")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_form_post_success(mock_glu,
                                 mock_async_session,
                                 mock_send_email,
                                 monkeypatch):
    mock_glu.get_attributes_from_ldap.side_effect = [
        TEST_EMAIL,
    ]

    mock_glu.get_group_from_ldap.return_value = set()

    mock_result = MagicMock()
    mock_result.scalar_one_or_none.return_value = None
    (mock_async_session.return_value.__aenter__.return_value
        .execute.return_value) = mock_result

    monkeypatch.setattr(settings, "EXCLUDED_USERS",
                        EXCLUDED_USERS.split(","))

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.post("/", data={"username": TEST_USERNAME})

    assert response.status_code == 200

    mock_token = (mock_async_session.return_value.__aenter__.
                  return_value.add.call_args[0][0])
    mock_send_email.assert_called_once_with(TEST_EMAIL, mock_token.token)

    (mock_async_session.return_value.__aenter__.return_value.
     add.assert_called_once())
    (mock_async_session.return_value.__aenter__.return_value.
     commit.assert_called_once())

    assert mock_token.username == TEST_USERNAME
    assert mock_token.expired == 0
    assert mock_token.claimed == 0


@pytest.mark.asyncio
async def test_form_post_locked_account(mock_glu):
    mock_glu.get_attributes_from_ldap.side_effect = [
        TEST_EMAIL,
    ]

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.post("/", data={"username": TEST_USERNAME})

    assert response.status_code == 200
    assert "If the username you specified is correct" in response.text


@pytest.mark.asyncio
async def test_form_post_infrateam_member(mock_glu):
    mock_glu.get_attributes_from_ldap.side_effect = [
        TEST_EMAIL,
    ]
    mock_glu.get_group_from_ldap.return_value = {TEST_USERNAME}

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.post("/", data={"username": TEST_USERNAME})

    assert response.status_code == 200
    assert "If the username you specified is correct" in response.text


@pytest.mark.asyncio
async def test_reset_get_valid_token(mock_async_session, mock_token):
    mock_token.username = TEST_USERNAME
    mock_token.claimed = 0
    mock_token.expired = 0

    mock_result = MagicMock()
    mock_result.scalar_one_or_none.return_value = mock_token
    (mock_async_session.return_value.__aenter__.return_value.
     execute.return_value) = mock_result

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.get(f"/reset/{mock_token.token}")

    assert response.status_code == 200
    assert "Your new GNOME Account password" in response.text


@pytest.mark.asyncio
async def test_reset_get_invalid_token(mock_async_session, mock_token):
    mock_result = MagicMock()
    mock_result.scalar_one_or_none.return_value = None
    (mock_async_session.return_value.__aenter__.return_value.
     execute.return_value) = mock_result

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.get(f"/reset/{mock_token.token}")

    assert response.status_code == 200
    assert "The token you specified is incorrect" in response.text


@pytest.mark.asyncio
async def test_reset_post_success(mock_async_session, mock_glu, mock_token):
    mock_glu.get_attributes_from_ldap.side_effect = [
        "FALSE"
     ]

    mock_token.username = TEST_USERNAME
    mock_token.claimed = 0
    mock_token.expired = 0

    mock_result = MagicMock()
    mock_result.scalar_one_or_none.return_value = mock_token
    (mock_async_session.return_value.__aenter__.return_value.execute.
     return_value) = mock_result

    mock_glu.replace_ldap_password.return_value = True

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.post(
            f"/reset/{mock_token.token}",
            data={"password": "newpassword"}
        )

    assert response.status_code == 200
    assert "Your GNOME Account password has been successfully" in response.text
    (mock_async_session.return_value.__aenter__.return_value.
     commit.assert_called_once())


@pytest.mark.asyncio
async def test_reset_post_ldap_error(mock_async_session, mock_glu, mock_token):
    mock_glu.get_attributes_from_ldap.side_effect = [
        "FALSE"
     ]

    mock_token.username = TEST_USERNAME

    mock_result = MagicMock()
    mock_result.scalar_one_or_none.return_value = mock_token
    (mock_async_session.return_value.__aenter__.return_value
     .execute.return_value) = mock_result

    mock_glu.replace_ldap_password.side_effect = Exception("LDAP Error")

    async with httpx.AsyncClient(
        transport=httpx.ASGITransport(
            app=app),
            base_url="http://test"
            ) as client:
        response = await client.post(
            f"/reset/{mock_token.token}",
            data={"password": "newpassword"}
        )

    assert response.status_code == 200
    assert "There was an error updating your password" in response.text
    (mock_async_session.return_value.__aenter__.return_value
     .rollback.assert_called_once())
