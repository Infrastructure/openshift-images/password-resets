FROM python:3.11-slim

EXPOSE 8000
ENV PYTHONPATH=/app

RUN apt-get update && apt-get install git gcc libldap2-dev \
    libsasl2-dev libpq-dev -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./app /app
COPY ./ipa-ca.crt /etc/ipa/ca.crt
RUN pip install -r /app/requirements.txt

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
