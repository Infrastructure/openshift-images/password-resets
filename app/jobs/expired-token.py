#!/usr/bin/python3

from helpers.db.db import Token
from helpers.db.base import sync_session as Session
from datetime import datetime, timedelta


def expired_token():
    tokens = Session.query(Token).all()

    for t in tokens:
        if not t.claimed:
            if datetime.now() > t.date + timedelta(hours=1):
                t.expired = 1

    Session.commit()
    Session.close()


expired_token()
