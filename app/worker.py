import os
import dramatiq
import smtplib

from dramatiq.brokers.redis import RedisBroker
from email.mime.multipart import MIMEMultipart
from app.helpers.glu import gnome_ldap_utils
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from jinja2 import Template
from app.helpers.settings.config import settings


redis_broker = RedisBroker(host=settings.REDIS_HOST, port=6379)
dramatiq.set_broker(redis_broker)

glu = gnome_ldap_utils.GnomeLdapUtils(
    settings.LDAP_GROUP_BASE, settings.LDAP_HOST, settings.LDAP_USER_BASE,
    settings.LDAP_USER, settings.LDAP_PASSWORD, settings.LDAP_CA_PATH
)


@dramatiq.actor
def send_email(email: str, token: str):
    msg_root = MIMEMultipart('alternative')
    msg_root['Subject'] = 'GNOME Account Password Reset'
    msg_root['From'] = 'GNOME Accounts <noreply@gnome.org>'
    msg_root['To'] = email
    msg_root['Reply-To'] = 'gnome-sysadmin@gnome.org'

    with open(os.path.join(
        settings.DOCUMENT_ROOT,
        'templates/password_reset_mail.txt'),
        'r',
        encoding='utf-8'
    ) as txt_file:
        plain_content = Template(txt_file.read()).render(token=token)

    with open(os.path.join(
        settings.DOCUMENT_ROOT,
        'templates/password_reset_mail.html'),
        'r',
        encoding='utf-8'
    ) as html_file:
        html_content = Template(html_file.read()).render(token=token)

    msg_root.attach(MIMEText(plain_content, 'plain'))

    msg_related = MIMEMultipart('related')
    msg_related.attach(MIMEText(html_content, 'html'))

    def attach_image(path, cid):
        with open(os.path.join(settings.DOCUMENT_ROOT,
                               path), 'rb') as img_file:
            img_data = img_file.read()
            img = MIMEImage(img_data)
            img.add_header('Content-ID', cid)
            msg_related.attach(img)

    attach_image('static/images/GNOME-LogoHorizontal-white.png', '<image1>')
    attach_image('static/images/Mail-LockSymbol.png', '<image2>')

    msg_root.attach(msg_related)

    with smtplib.SMTP(settings.SMTP_RELAY_HOST, 587) as server:
        server.starttls()
        server.send_message(msg_root)
        server.quit()


@dramatiq.actor
def activate_account(username: str):
    glu.set_nsaccountlock(username, False)
    glu.add_user_to_ldap_group(username, 'ipausers')
