#!/usr/bin/python3

from sqlalchemy import Column, String, Integer, DateTime
from helpers.db.base import Base


class Token(Base):
    __tablename__ = 'tokens'

    id = Column(Integer, primary_key=True)
    username = Column(String(20))
    token = Column(String(32))
    claimed = Column(Integer)
    expired = Column(Integer)
    date = Column(DateTime)

    def __init__(self, username, token, claimed, expired, date):
        self.username = username
        self.token = token
        self.claimed = claimed
        self.expired = expired
        self.date = date
