CREATE TABLE tokens (
    id SERIAL PRIMARY KEY,
    username VARCHAR(20) NOT NULL,
    token VARCHAR(32) NOT NULL,
    claimed INTEGER NOT NULL,
    expired INTEGER NOT NULL,
    date TIMESTAMP NOT NULL
);
