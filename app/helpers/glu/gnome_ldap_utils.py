#!/usr/bin/python3

import ldap
import ldap.filter
import sys


class GnomeLdapUtils:
    def __init__(self, ldap_group_base, ldap_host, ldap_user_base,
                 ldap_user, ldap_password, ldap_ca_path='/etc/ipa/ca.crt'):

        self.ldap_group_base = ldap_group_base
        self.ldap_user_base = ldap_user_base
        self.ldap_user = ldap_user
        self.ldap_password = ldap_password
        self.ldap_host = ldap_host
        self.ldap_ca_path = ldap_ca_path

        try:
            ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, self.ldap_ca_path)

            self.conn = ldap.ldapobject.ReconnectLDAPObject(
                f"ldaps://{self.ldap_host}:636"
            )
            self.conn.simple_bind_s(self.ldap_user, self.ldap_password)
        except ldap.LDAPError as e:
            print(e, file=sys.stderr)
            sys.exit(1)

    def get_attributes_from_ldap(self, uid, attr, *attrs):
        results = []
        ldap_filter = ldap.filter.filter_format('(uid=%s)', (uid,))

        if attrs:
            attrs = ['uid', attr] + list(attrs)
            result = self.conn.search_s(self.ldap_user_base,
                                        ldap.SCOPE_SUBTREE,
                                        ldap_filter,
                                        attrs
                                        )

            for arg in attrs:
                try:
                    results.append(result[0][1][arg][0].decode('UTF-8'))
                except KeyError:
                    results.append(None)
        else:
            result = self.conn.search_s(self.ldap_user_base,
                                        ldap.SCOPE_SUBTREE,
                                        ldap_filter,
                                        ('uid', attr)
                                        )

        if results:
            return results
        elif result:
            try:
                return result[0][1][attr][0].decode('UTF-8')
            except KeyError:
                return None
        else:
            return None

    def get_group_from_ldap(self, group):
        filter_str = ldap.filter.filter_format(
            '(&(objectClass=groupofnames)(cn=%s))', (group,)
        )
        results = self.conn.search_s(
            self.ldap_group_base, ldap.SCOPE_SUBTREE, filter_str, ('member',)
        )

        members = set()

        if results and len(results[0][1]) > 0:
            for _, attr in results:
                for userid in attr['member']:
                    splitentry = userid.decode('utf-8').split(',')
                    singleentry = splitentry[0]
                    splitteduid = singleentry.split('=')
                    uid = splitteduid[1]
                    members.add(uid)

        return members

    def replace_ldap_password(self, userid, password):
        replace_password = [(ldap.MOD_REPLACE, 'userPassword',
                             password.encode('utf-8'))]
        self.conn.modify_s(
            f'uid={userid},{self.ldap_user_base}', replace_password
        )

    def set_nsaccountlock(self, userid, lock=True):
        lock_value = 'TRUE' if lock else 'FALSE'
        mod_attrs = [(ldap.MOD_REPLACE, 'nsaccountlock',
                      lock_value.encode('utf-8'))]
        self.conn.modify_s(
            f'uid={userid},{self.ldap_user_base}', mod_attrs
        )

    def add_user_to_ldap_group(self, userid, group):
        uid_query = f"uid={userid},{self.ldap_user_base}".encode()
        add_members = [(ldap.MOD_ADD, 'member', uid_query)]
        try:
            self.conn.modify_s(
                f'cn={group},{self.ldap_group_base}',
                add_members
            )
        except (ldap.ALREADY_EXISTS, ldap.TYPE_OR_VALUE_EXISTS):
            pass
