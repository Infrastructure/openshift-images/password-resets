import os


class Settings:
    def __init__(self):
        self.LDAP_GROUP_BASE = os.getenv('LDAP_GROUP_BASE', '')
        self.LDAP_HOST = os.getenv('LDAP_HOST', '')
        self.LDAP_USER_BASE = os.getenv('LDAP_USER_BASE', '')
        self.LDAP_USER = os.getenv('LDAP_USER', '')
        self.LDAP_PASSWORD = os.getenv('LDAP_PASSWORD', '')
        self.LDAP_CA_PATH = os.getenv('LDAP_CA_PATH', '')

        self.SMTP_RELAY_HOST = os.getenv('SMTP_RELAY_HOST', '')

        self.SENTRY_DSN = os.getenv('SENTRY_DSN', '')

        self.DOCUMENT_ROOT = os.getenv('DOCUMENT_ROOT',
                                       os.path.abspath(
                                           os.path.dirname(__file__)
                                           + '/../../'
                                           ))

        self.REDIS_HOST = os.getenv('REDIS_HOST', 'redis')

        self.EXCLUDED_USERS = os.getenv('EXCLUDED_USERS', '').split(',')


settings = Settings()
