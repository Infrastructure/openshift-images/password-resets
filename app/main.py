#!/usr/bin/env python3

import os
import secrets
import datetime
import uvicorn
import sentry_sdk

from fastapi import FastAPI, Request, Form
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from sqlalchemy import select
from app.helpers.db.db import Token
from app.helpers.db.base import async_session
from app.helpers.glu import gnome_ldap_utils
from app.worker import send_email, activate_account
from app.helpers.settings.config import settings

if settings.SENTRY_DSN:
    sentry_sdk.init(
      dsn=settings.SENTRY_DSN,
      traces_sample_rate=1.0,
      _experiments={
              "continuous_profiling_auto_start": True
              },
      )

glu = gnome_ldap_utils.GnomeLdapUtils(
    settings.LDAP_GROUP_BASE, settings.LDAP_HOST, settings.LDAP_USER_BASE,
    settings.LDAP_USER, settings.LDAP_PASSWORD, settings.LDAP_CA_PATH
)

app = FastAPI()
app.mount("/static", StaticFiles(directory=os.path.join(
    settings.DOCUMENT_ROOT, "static")),
    name="static"
    )

templates = Jinja2Templates(directory=os.path.join(
    settings.DOCUMENT_ROOT,
    "templates")
    )


@app.get("/")
def form_get(request: Request):
    return templates.TemplateResponse(request, 'index.html')


@app.post("/")
async def form_post(request: Request, username: str = Form(...)):
    mail = glu.get_attributes_from_ldap(username, 'mail')

    if mail:
        infrateam = set.union(
            glu.get_group_from_ldap('accounts'),
            glu.get_group_from_ldap('sysadmin'),
            glu.get_group_from_ldap('admins')
        )

        if (username not in infrateam and
                username not in settings.EXCLUDED_USERS):
            async with async_session() as session:
                token_query = select(Token).where(
                    Token.username == username,
                    Token.expired == 0,
                    Token.claimed == 0
                )
                result = await session.execute(token_query)
                token_exists = result.scalar_one_or_none()

                if token_exists:
                    return templates.TemplateResponse(
                        request,
                        'general-form.html',
                        context={
                            'badtoken': True
                            }
                    )

                date = datetime.datetime.now()
                token = secrets.token_hex(16)

                _token = Token(username=username, token=token,
                               expired=0, claimed=0,
                               date=date)
                session.add(_token)
                await session.commit()

                send_email.send(mail, token)

    return templates.TemplateResponse(request,
                                      'general-form.html',
                                      context={
                                          'submitted': True
                                          })


@app.get("/reset/{token}")
async def form_reset_get(request: Request, token: str):
    async with async_session() as session:
        token_query = select(Token).where(Token.token == token)
        result = await session.execute(token_query)
        t = result.scalar_one_or_none()

        if t and not (t.claimed or t.expired):
            return templates.TemplateResponse(request, 'form-reset.html')

    return templates.TemplateResponse(request,
                                      'general-form.html',
                                      context={
                                          'badtoken': True
                                          })


@app.post("/reset/{token}")
async def form_reset_post(request: Request, token: str,
                          password: str = Form(...)):
    async with async_session() as session:
        token_query = select(Token).where(Token.token == token)
        result = await session.execute(token_query)
        t = result.scalar_one_or_none()

        if t:
            try:
                glu.replace_ldap_password(t.username, password)

                t.claimed = 1
                await session.commit()

                is_locked = glu.get_attributes_from_ldap(
                    t.username, 'nsaccountlock'
                )

                if is_locked == 'TRUE':
                    activate_account.send(t.username)

                return templates.TemplateResponse(request,
                                                  'general-form.html',
                                                  context={
                                                      'passwordsuccess': True
                                                    })
            except Exception:
                await session.rollback()
                return templates.TemplateResponse(request,
                                                  'general-form.html',
                                                  context={
                                                      'passworderror': True
                                                      })

    return templates.TemplateResponse(request,
                                      'general-form.html',
                                      context={
                                          'badtoken': True
                                          })


if __name__ == '__main__':
    uvicorn.run(app)
